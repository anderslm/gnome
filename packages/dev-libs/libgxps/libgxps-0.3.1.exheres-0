# Copyright 2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="XPS Document Library"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    lcms
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt [[ note = [ required for man page generation ] ]]
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.10.1] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        app-arch/libarchive[>=2.8.0]
        dev-libs/glib:2[>=2.36]
        media-libs/freetype:2
        media-libs/libpng:= [[ note = [ automagic dependency ] ]]
        x11-libs/cairo[>=1.10.0]
        lcms? ( media-libs/lcms2 )
        tiff? ( media-libs/tiff )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    test:
        x11-libs/gtk+:3
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dwith-libjpeg=true'
    '-Denable-man=true'
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( '!gobject-introspection disable-introspection' )
MESON_SRC_CONFIGURE_OPTION_WITHS=(
    'lcms liblcms2'
    'tiff libtiff'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Denable-test=true -Denable-test=false'
)

