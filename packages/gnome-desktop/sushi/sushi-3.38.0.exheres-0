# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require meson

SUMMARY="A quick previewer for Nautilus"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
  wayland
  X
    ( linguas: an ar as ast be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gl
               gu he hi hu id it ja kn ko lt lv ml mr nb nl or pa pl pt pt_BR ru sk sl sr sr@latin
               sv ta te tg th tr ug uk uz@cyrillic vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        sys-devel/libtool
        virtual/pkg-config[>=0.22]
    build+run:
        dev-libs/glib:2[>=2.29.14]
        gnome-bindings/gjs:1[>=1.38.0]
        gnome-desktop/evince[gobject-introspection]
        gnome-desktop/gobject-introspection:1[>=1.0.1]
        gnome-desktop/gtksourceview:3.0[gobject-introspection]
        media-libs/clutter-gst:3.0[gobject-introspection]
        media-libs/freetype:2
        media-libs/gstreamer:1.0[gobject-introspection]
        media-plugins/gst-plugins-base:1.0[gobject-introspection][X?][wayland?] [[ note = [ for gstreamer-tag ] ]]
        media-libs/libmusicbrainz:5
        net-libs/webkit:4.0[gobject-introspection][X?][wayland?]
        x11-libs/clutter:1[>=1.11.4][gobject-introspection][X?][wayland?]
        x11-libs/clutter-gtk:1.0[>=1.0.1][gobject-introspection]
        x11-libs/gdk-pixbuf[>=2.23.0][gobject-introspection][X?]
        x11-libs/gtk+:3[>=3.13.2][gobject-introspection][X?][wayland?]
        x11-libs/harfbuzz[>=0.9.9]
"
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'X X11'
    'wayland'
)
