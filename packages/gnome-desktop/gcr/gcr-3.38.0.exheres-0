# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gcr

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    ( linguas: af ar as ast az be be@latin bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka kn ko lt lv
               mai mg mk ml mn mr ms nb ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl sq sr
               sr@latin sv ta te tg th tr ug uk vi xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.4]
        dev-lang/vala:*
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        app-crypt/gnupg
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.4.5]
        dev-libs/p11-kit:1[>=0.19.0]
        sys-apps/dbus
        x11-libs/gtk+:3[>=3.12][gobject-introspection?][X]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.34.0] )
        !gnome-desktop/gnome-keyring:1[<3.3] [[
            description = [ gcr is now in a separate package, was part of gnome-keyring before ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Respect-CPP-overriding.patch
)
MESON_SRC_CONFIGURE_PARAMS=(
    '-Dgtk=true'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)

src_prepare() {
    meson_src_prepare
    edo sed -e "/'attributes'/d" -e "/'slot'/d" -i gck/meson.build
}
