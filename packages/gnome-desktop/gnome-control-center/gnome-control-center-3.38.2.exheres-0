# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] freedesktop-desktop gtk-icon-cache
require gsettings meson

SUMMARY="Configuration Applications for the GNOME Desktop"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cups   [[ description = [ add support for managing printers ] ]]
    cheese [[ description = [ add support for adding user account images using cheese ] ]]
    doc
    ibus [[ description = [ add support for the IBus input method ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.1]
        virtual/pkg-config[>=0.20]
        doc? ( dev-libs/libxslt )
    build+run:
        dev-libs/glib:2[>=2.56.0]
        dev-libs/libepoxy
        dev-libs/libhandy:1[>=0.90.0]
        dev-libs/libpwquality[>=1.2.2]
        dev-libs/libxml2:2.0
        gnome-desktop/colord-gtk[>=0.1.24]
        gnome-desktop/gnome-bluetooth:1[>=3.18.2]
        gnome-desktop/gnome-desktop:3.0[>=3.35.4]
        gnome-desktop/gnome-online-accounts[>=3.25.3]
        gnome-desktop/gnome-settings-daemon:3.0[>=3.27.90]
        gnome-desktop/grilo:0.3[>=0.3.0]
        gnome-desktop/gsettings-desktop-schemas[>=3.31.0]
        gnome-desktop/gsound
        gnome-desktop/libgnomekbd[>=2.91.91]
        gnome-desktop/libgtop:2
        gnome-desktop/libgudev[>=232]
        gnome-desktop/libsoup:2.4
        media-libs/fontconfig
        media-libs/libcanberra[providers:gtk3][>=0.13]
        media-sound/pulseaudio[>=2.0]
        net-apps/NetworkManager[>=1.24.0]
        net-libs/libnma[>=1.8.0] [[
            note = [ required for network applet ]
        ]]
        net-wireless/ModemManager[>=0.7]
        sys-apps/accountsservice[>=0.6.39]
        sys-apps/colord[>=0.1.34]
        sys-apps/upower[>=0.99.8]
        sys-auth/polkit:1[>=0.114]
        x11-libs/gdk-pixbuf:2.0[>=2.23.0]
        x11-libs/gtk+:3[>=3.22.20][wayland?]
        x11-libs/libX11
        x11-libs/libXi[>=1.2]
        x11-libs/libwacom[>=0.7]
        cheese? ( gnome-desktop/cheese[>=3.28.0] )
        cups? (
            net-fs/samba
            net-print/cups[>=1.4]
        )
        ibus? ( inputmethods/ibus[>=1.5.2] )
    test:
        dev-python/python-dbusmock
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/3.38/* )

MESON_SRC_CONFIGURE_PARAMS=( '-Dkerberos=false' '-Dprivileged_group=wheel' '-Dsnap=false' '-Dmalcontent=false' )

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'cheese'
    'cups'
    'doc documentation'
    'ibus'
)

# Needs X access
RESTRICT="test"

pkg_postinst() {
    freedesktop-desktop_update_desktop_database
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_update_desktop_database
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

