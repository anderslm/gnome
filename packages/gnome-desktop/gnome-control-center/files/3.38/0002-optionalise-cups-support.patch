From e947de67a6a0723ab9806a11212eb03218beeaba Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Mon, 28 Sep 2020 09:46:50 +0100
Subject: [PATCH] optionalise cups support

---
 meson.build                | 49 ++++++++++++++++++++++----------------
 meson_options.txt          |  1 +
 panels/meson.build         |  5 +++-
 shell/cc-panel-loader.c    |  4 ++++
 tests/printers/meson.build | 37 ++++++++++++++--------------
 5 files changed, 56 insertions(+), 40 deletions(-)

diff --git a/meson.build b/meson.build
index 6e31acfe1..b94bb1f1f 100644
--- a/meson.build
+++ b/meson.build
@@ -148,29 +148,35 @@ common_deps = [
   dependency('gtk+-3.0', version: '>= 3.22.20')
 ]
 
-# Check for CUPS 1.4 or newer
-cups_dep = dependency('cups', version : '>= 1.4', required: false)
-assert(cups_dep.found(), 'CUPS 1.4 or newer not found')
-
-# https://bugzilla.gnome.org/show_bug.cgi?id=696766
-cups_cflags = []
-if cups_dep.version().version_compare('>= 1.6')
-  cups_cflags += '-D_PPD_DEPRECATED='
-endif
-
-# cups headers
-check_headers = [
-  ['HAVE_CUPS_CUPS_H', 'cups/cups.h'],
-  ['HAVE_CUPS_PPD_H', 'cups/ppd.h']
-]
+# Optional dependency for the printers panel
+enable_cups = get_option('cups')
+if enable_cups
+  # Check for CUPS 1.4 or newer
+  cups_dep = dependency('cups', version : '>= 1.4', required: false)
+  assert(cups_dep.found(), 'CUPS 1.4 or newer not found')
+
+  # https://bugzilla.gnome.org/show_bug.cgi?id=696766
+  cups_cflags = []
+  if cups_dep.version().version_compare('>= 1.6')
+    cups_cflags += '-D_PPD_DEPRECATED='
+  endif
+
+  # cups headers
+  check_headers = [
+    ['HAVE_CUPS_CUPS_H', 'cups/cups.h'],
+    ['HAVE_CUPS_PPD_H', 'cups/ppd.h']
+  ]
 
-foreach header: check_headers
-  assert(cc.has_header(header[1], args: cups_cflags), 'CUPS headers not found: ' + header[1])
-endforeach
+  foreach header: check_headers
+    assert(cc.has_header(header[1], args: cups_cflags), 'CUPS headers not found: ' + header[1])
+  endforeach
 
-config_h.set10('HAVE_CUPS_HTTPCONNECT2',
-               cc.has_function('httpConnect2', dependencies: cups_dep),
-               description: 'Define if httpConnect2() is available in CUPS')
+  config_h.set10('HAVE_CUPS_HTTPCONNECT2',
+                 cc.has_function('httpConnect2', dependencies: cups_dep),
+                 description: 'Define if httpConnect2() is available in CUPS')
+endif
+config_h.set('HAVE_CUPS', enable_cups,
+    description: 'Defined if cups support is enabled')
 
 # Optional dependency for the user accounts panel
 enable_cheese = get_option('cheese')
@@ -302,6 +308,7 @@ output += '     Optimized .................................. ' + control_center_
 output += ' Panels \n'
 output += '     GNOME Bluetooth (Bluetooth panel) .......... ' + host_is_linux_not_s390.to_string() + '\n'
 output += '     Cheese (Users panel webcam support) ........ ' + enable_cheese.to_string() + '\n'
+output += '     Cups (Printer panel support) ..........;.... ' + enable_cups.to_string() + '\n'
 output += '     IBus (Region panel IBus support) ........... ' + enable_ibus.to_string() + '\n'
 output += '     NetworkManager (Network panel) ............. ' + host_is_linux.to_string() + '\n'
 output += '     Wacom (Wacom tablet panel) ................. ' + host_is_linux_not_s390.to_string() + '\n'
diff --git a/meson_options.txt b/meson_options.txt
index 1b7b54810..d7fa4e2ad 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -1,4 +1,5 @@
 option('cheese', type: 'boolean', value: true, description: 'build with cheese webcam support')
+option('cups', type: 'boolean', value: true, description: 'build with cups support')
 option('documentation', type: 'boolean', value: false, description: 'build documentation')
 option('ibus', type: 'boolean', value: true, description: 'build with IBus support')
 option('privileged_group', type: 'string', value: 'wheel', description: 'name of group that has elevated permissions')
diff --git a/panels/meson.build b/panels/meson.build
index 2f4fdc5e3..ba627caef 100644
--- a/panels/meson.build
+++ b/panels/meson.build
@@ -18,7 +18,6 @@ panels = [
   'notifications',
   'online-accounts',
   'power',
-  'printers',
   'region',
   'removable-media',
   'search',
@@ -29,6 +28,10 @@ panels = [
   'user-accounts'
 ]
 
+if enable_cups
+  panels += ['printers']
+endif
+
 if host_is_linux
   panels += ['network']
 endif
diff --git a/shell/cc-panel-loader.c b/shell/cc-panel-loader.c
index f20384394..c85be631a 100644
--- a/shell/cc-panel-loader.c
+++ b/shell/cc-panel-loader.c
@@ -50,7 +50,9 @@ extern GType cc_wifi_panel_get_type (void);
 extern GType cc_notifications_panel_get_type (void);
 extern GType cc_goa_panel_get_type (void);
 extern GType cc_power_panel_get_type (void);
+#ifdef HAVE_CUPS
 extern GType cc_printers_panel_get_type (void);
+#endif /* HAVE_CUPS */
 extern GType cc_region_panel_get_type (void);
 extern GType cc_removable_media_panel_get_type (void);
 extern GType cc_search_panel_get_type (void);
@@ -114,7 +116,9 @@ static CcPanelLoaderVtable default_panels[] =
   PANEL_TYPE("notifications",    cc_notifications_panel_get_type,        NULL),
   PANEL_TYPE("online-accounts",  cc_goa_panel_get_type,                  NULL),
   PANEL_TYPE("power",            cc_power_panel_get_type,                NULL),
+#ifdef HAVE_CUPS
   PANEL_TYPE("printers",         cc_printers_panel_get_type,             NULL),
+#endif /* HAVE_CUPS */
   PANEL_TYPE("region",           cc_region_panel_get_type,               NULL),
   PANEL_TYPE("removable-media",  cc_removable_media_panel_get_type,      NULL),
   PANEL_TYPE("search",           cc_search_panel_get_type,               NULL),
diff --git a/tests/printers/meson.build b/tests/printers/meson.build
index 60f144fa6..f27e92190 100644
--- a/tests/printers/meson.build
+++ b/tests/printers/meson.build
@@ -1,22 +1,23 @@
 
-test_units = [
-  #'test-canonicalization',
-  'test-shift'
-]
+if enable_cups
+  test_units = [
+    #'test-canonicalization',
+    'test-shift'
+  ]
 
-includes = [top_inc, include_directories('../../panels/printers')]
-cflags = '-DTEST_SRCDIR="@0@"'.format(meson.current_source_dir())
+  includes = [top_inc, include_directories('../../panels/printers')]
+  cflags = '-DTEST_SRCDIR="@0@"'.format(meson.current_source_dir())
 
-foreach unit: test_units
-  exe = executable(
-                    unit,
-           [unit + '.c'],
-    include_directories : includes,
-           dependencies : common_deps,
-              link_with : [printers_panel_lib],
-                 c_args : cflags
-  )
-
-  test(unit, exe)
-endforeach
+  foreach unit: test_units
+    exe = executable(
+                      unit,
+             [unit + '.c'],
+      include_directories : includes,
+             dependencies : common_deps,
+                link_with : [printers_panel_lib],
+                   c_args : cflags
+    )
 
+    test(unit, exe)
+  endforeach
+endif
-- 
2.28.0

