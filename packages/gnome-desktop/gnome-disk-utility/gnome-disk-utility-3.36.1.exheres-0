# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache
require gsettings freedesktop-desktop freedesktop-mime
require meson

SUMMARY="Disk Utility for GNOME"
HOMEPAGE="https://www.gnome.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="systemd"

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        dev-libs/libxslt [[ note = [ for xsltproc ] ]]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.20]
    build+run:
        app-arch/xz[>=5.0.5] [[ note = [ for liblzma ] ]]
        dev-libs/glib:2[>=2.31.0]
        dev-libs/libsecret:1[>=0.7]
        dev-libs/libpwquality[>=1.0]
        gnome-desktop/gnome-settings-daemon:3.0[>=3.8]
        media-libs/libcanberra[>=0.1][providers:gtk3]
        media-libs/libdvdread[>=4.2.0]
        sys-apps/udisks:2[>=2.7.6]
        x11-libs/gtk+:3[>=3.16.0]
        x11-libs/libnotify[>=0.7]
        systemd? ( sys-apps/systemd[>=209] )
"

MESON_SRC_CONFIGURE_PARAMS=( '-Denable-gsd-plugin=true' '-Dman=true' )
MESON_SRC_CONFIGURE_OPTIONS=(
    'systemd -Dlogind=libsystemd -Dlogind=none'
)

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

