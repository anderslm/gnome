# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="Desktop environment for GNOME"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da
               de dz el en en_CA en_GB en@shaw eo es et eu fa fi fr fur fy ga gl gu ha he hi hr hu
               hy id ig is it ja ka kg kk km kn ko ku ky li lo lt lv mai mg mi mk ml mn mr ms nb
               nds ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te
               tg th tk tr ug uk ur uz uz@cyrillic vi wa xh yi yo zh_CN zh_HK zh_TW zu )
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.18]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.53.0]
        gnome-desktop/gobject-introspection:1[>=0.9.7]
        gnome-desktop/gsettings-desktop-schemas[>=3.27.0][gobject-introspection]
        sys-apps/bubblewrap
        sys-devel/gettext[>=0.19.8]
        sys-libs/libseccomp
        x11-apps/xkeyboard-config
        x11-libs/gdk-pixbuf:2.0[>=2.36.5][gobject-introspection]
        x11-libs/gtk+:3[>=3.3.6][gobject-introspection]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

AT_M4DIR=( m4 )

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgnome_distributor=Exherbo
    -Ddesktop_docs=false
    -Dinstalled_tests=false
    -Dudev=enabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=( 'providers:systemd' )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gtk-doc' )

# Tries to connect to wayland socket
RESTRICT="test"

src_test() {
    unset DISPLAY
    mesom_src_tests
}

