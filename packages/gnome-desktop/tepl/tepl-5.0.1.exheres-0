# Copyright 2020 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require xdummy [ phase=test ]
require meson

SUMMARY="Text editor product line"
DESCRIPTION="
Tepl is a library that eases the development of GtkSourceView-based text editors and IDEs.
"
HOMEPAGE="https://wiki.gnome.org/Projects/Tepl"

LICENCES="LGPL-3"
SLOT="5"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        sys-devel/gettext
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/amtk:5[>=5.0.0][gobject-introspection]
        dev-libs/glib:2[>=2.64.0]
        dev-libs/icu:=
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gtksourceview:4.0[>=4.0.0][gobject-introspection]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
    test:
        x11-libs/gtk+:3[X]
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)

src_test() {
    HOME=${HOME%/} # Workaround for https://gitlab.gnome.org/GNOME/tepl/-/issues/19

    xdummy_start
    meson_src_test
    xdummy_stop
}

